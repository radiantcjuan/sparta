<div class="container-fluid side-nav position-fixed">
	<button class="btn btn-dark mt-3 btn-close-nav"><i class="fa fa-close"></i></button>	
	<div class="sparta-logo"></div>	
	<ul>
		<li class="nav-item">
			
			<a href="{{route('dashboard')}}" class="nav-link rounded-0  {{ (\Route::currentRouteName() == 'dashboard') ? 'active' : '' }}"><i class="fa fa-dashboard"></i> Dashboard</a>
		</li>
		<li class="nav-item">
			<a href="{{route('leaderboard',['type'=>'sales'])}}" class="nav-link rounded-0 {{ (\Route::currentRouteName() == 'leaderboard') ? 'active' : '' }}"><i class="fa fa-trophy"></i> Leaderboard</a>
		</li>
		@if($gRole->name == 'Admin')
			<li class="nav-item">
				<a href="{{route('roulette',['round'=>'round1'])}}" class="nav-link rounded-0  {{ (\Route::currentRouteName() == 'roulette') ? 'active' : '' }}"><i class="fa fa-gamepad"></i> Roulette</a>
			</li>
			<li class="nav-item">
				<a href="{{route('users.index')}}" class="nav-link rounded-0  {{ (\Route::currentRouteName() == 'users.index') ? 'active' : '' }}"><i class="fa fa-users"></i> Users</a>
			</li>
			<li class="nav-item">
				<a href="{{route('advisory.index')}}" class="nav-link rounded-0  {{ (\Route::currentRouteName() == 'advisory.index') ? 'active' : '' }}"><i class="fa fa-info-circle"></i> Advisories</a>
			</li>
		@endif
	</ul>
</div>