@extends('home')

{{-- for custom css in a specific page --}}
@section('custom-styles')
@endsection

@section('cms-content')
    @include('layouts.head-title',array('title'=>'Roles','isAjax'=>0,'url'=>route('roles.create'),'create'=>1,'parent'=>route('roles')))
    <div class="container-fluid pt-4">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        <div class="col-12">
            <form action="{{ route('roles.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="card">
                    <div class="card-header">
                        Add Role
                    </div>
                    <div class="card-body">
                            <div class=form-group>
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name">
                            </div>
                            <div class=form-group>
                                <div class="input-group">
                                    <label for="status">Enabled <input type="checkbox" name="status"></label>
                                </div>
                            </div>
                    </div>
                    
                    <div class="card-footer">
                        <input type="submit" class="btn btn-success" value="Submit">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

{{-- for custom JS and js declarations in a specific page --}}
@section('custom-js')
@endsection