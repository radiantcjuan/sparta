@extends('home')

{{-- for custom css in a specific page --}}
@section('custom-styles')
@endsection

@section('cms-content')
    @include('layouts.head-title',array('title'=>'Roles','isAjax'=>0,'url'=>route('roles.create'),'create'=>1))
    <div class="container-fluid pt-4">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
        <div class="col-12 py-2">
            <table class="table table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Created at</th>
                        <th>Updated at</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $roles)
                        <tr>
                            <td>
                                {{$roles->name}}
                            </td>
                            <td>
                                {{$roles->status}}
                            </td>
                            <td>
                                {{$roles->created_at}}
                            </td>
                            <td>
                                {{$roles->updated_at}}
                            </td>
                            <td class="text-left">
                                <a href={{route('roles.edit',array('id'=>$roles->id))}} class="btn-primary btn btn-edit"><i class="fa fa-edit"></i></a>
                                <a href="javascript:void(0);" class="btn-danger btn btn-delete" data-id="{{$roles->id}}" data-name="{{$roles->name}}"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                   
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="delete-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('roles.delete')}}" method="post" id="delete-form">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <p> Are you sure you want to delete <span class="delete-item-name"></span>?</p>
                        <input type="hidden" name="menu_id_delete">
                    </form>
                </div>
                <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-delete-item">Delete</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- for custom JS and js declarations in a specific page --}}
@section('custom-js')
<script>
    $(document).ready(function(){

        $('.btn-delete').click(function(){
            var id = $(this).data('id');
            var name = $(this).data('name');

            $('.delete-item-name').text(name);
            $('input[name="menu_id_delete"]').val(id);
            $('#delete-modal').modal('show');
        });

        $('.btn-delete-item').click(function(){
            $('#delete-form').submit();
        });
    });
</script>
@endsection