@extends('layouts.app')

@section('content')
    <div class="users-container">
        @include('layouts.head-title',array('title'=>'Users','isAjax'=>0,'url'=>route('users.create'),'create'=>1))
        <div class="container-fluid pt-4">
            <div class="users-container-inner p-4">
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                <div class="col-12">
                    <table class="table table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Status</th>
                                <th>Created at</th>
                                <th>Updated at</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $users)
                                <tr>
                                    <td>
                                        <div class="profile-pic" style="background-image:url({{ \Storage::url($users->profile_pic) }});"></div>
                                    </td>
                                    <td>
                                        {{$users->name}}
                                    </td>
                                    <td>
                                        {{$users->email}}
                                    </td>
                                    <td>
                                        {{$users->role['name']}}
                                    </td>
                                    <td>
                                        {{$users->status}}
                                    </td>
                                    <td>
                                        {{$users->created_at}}
                                    </td>
                                    <td>
                                        {{$users->updated_at}}
                                    </td>
                                    <td class="text-left">
                                        <a href={{route('users.edit',array('id'=>$users->id))}} class="btn-primary btn btn-edit"><i class="fa fa-edit"></i></a>
                                        <a href="javascript:void(0);" class="btn-danger btn btn-delete" data-id="{{$users->id}}" data-name="{{$users->name}}"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="modal fade" tabindex="-1" role="dialog" id="delete-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Delete</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('users.delete')}}" method="post" id="delete-form">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <p> Are you sure you want to delete <span class="delete-item-name"></span>?</p>
                            <input type="hidden" name="menu_id_delete">
                        </form>
                    </div>
                    <div class="modal-footer">
                            <button type="button" class="btn btn-danger btn-delete-item">Delete</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- for custom JS and js declarations in a specific page --}}
@section('scripts')
    <script src="/js/admin/user.js"></script>
@endsection