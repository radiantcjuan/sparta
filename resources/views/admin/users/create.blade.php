@extends('layouts.app')

@section('content')
    <div class="users-container w-50 container-fluid">
        @include('layouts.head-title',array('title'=>'Users','isAjax'=>0,'url'=>route('users.create'),'create'=>1,'parent'=>route('users.index')))
        <div class="container-fluid pt-4">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            <div class="col-12">
                <form action="{{ route('users.store') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header">
                            Add User
                        </div>
                        <div class="card-body">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class=form-group>
                                            <label for="name">Upload Profile Picture</label>
                                            <img src="/images/profile-placeholder.png" alt="image preview" class="d-block mb-2 mt-2" id="imgInp">
                                            <input type="file" class="form-control" name="profile_pic">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class=form-group>
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" name="name">
                                        </div>
                                        <div class=form-group>
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control" name="password">
                                        </div>
                                        <div class=form-group>
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" name="email">
                                        </div>
                                        <div class=form-group>
                                            <label for="role">Role</label>
                                            <select name="role" class="form-control">
                                                <option value="">Select Role</option>
                                                @foreach($roles as $role)
                                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" class="btn btn-success" value="Submit">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

{{-- for custom JS and js declarations in a specific page --}}
@section('scripts')
<script>
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#imgInp').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $('input[name="profile_pic"]').change(function() {
        readURL(this);
    });
</script>
@endsection