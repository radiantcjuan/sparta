@extends('layouts.app')

@section('content')
@php
    $userss = \Auth::user();
    $gRole = \App\Role::find($userss->role_id);

@endphp
<div class="users-container w-50 container-fluid">
    @include('layouts.head-title',array('title'=>'Users','isAjax'=>0,'url'=>route('users.create'),'create'=>1,'parent'=>($gRole->name == 'Admin') ? route('users.index') : route('dashboard')  ))
    <div class="container-fluid pt-4">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
        <div class="col-12">
            <form action="{{ route('users.edit',array('id'=>$user->id)) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="card">
                    <div class="card-header">
                        Edit
                    </div>
                    <div class="card-body">   
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class=form-group>
                                        <label for="name">Upload Profile Picture</label>
                                        <img src="{{ ($user->profile_pic) ? \Storage::url($user->profile_pic) : '/images/profile-placeholder.png' }}" alt="image preview" class="d-block mb-2 mt-2" id="imgInp">
                                        <input type="file" class="form-control" name="profile_pic">
                                        <input type="hidden" name="hidden_profile_pic" value="{{$user->profile_pic}}">
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class=form-group>
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" name="name" value="{{$user->name}}">
                                    </div>
                                    <div class=form-group>
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" name="password">
                                        <input type="hidden" name="hidden_password" value={{$user->password}}>
                                    </div>
                                    <div class=form-group>
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" name="email" value="{{$user->email}}">
                                    </div>
                                    <div class=form-group>
                                        <label for="role">Role</label>
                                        <select name="role" class="form-control">
                                            <option value="">Select Role</option>
                                            @foreach($roles as $role)
                                                <option value="{{$role->id}}" {{ ($role->id == $user->role_id) ? 'selected' : '' }}>{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class=form-group>
                                        <div class="input-group">
                                            <label for="status">Enabled <input type="checkbox" name="status" value="enabled" {{($user->status) ? 'checked' : '' }}></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="card-footer">
                        <input type="submit" class="btn btn-success" value="Submit">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

{{-- for custom JS and js declarations in a specific page --}}
@section('scripts')
<script>
    
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#imgInp').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $('input[name="profile_pic"]').change(function() {
        readURL(this);
    });
</script>
@endsection