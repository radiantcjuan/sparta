@extends('home')

{{-- for custom css in a specific page --}}
@section('custom-styles')
@endsection

@section('cms-content')
    @include('layouts.head-title',array('title'=>'Menus','isAjax'=>1,'url'=>'','create'=>1))
    <div class="container-fluid pt-4">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
        <div class="col-12 py-2">
            <table class="table table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th>Name</th>
                        <th>Key</th>
                        <th>Enabled</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $menus)
                        <tr>
                            <td>
                                {{$menus->menu_name}}
                            </td>
                            <td>
                                {{$menus->menu_key}}
                            </td>
                            <td>
                                {{$menus->enabled}}
                            </td>
                            <td>
                                {{$menus->created_at}}
                            </td>
                            <td>
                                {{$menus->updated_at}}
                            </td>
                            <td class="text-left">
                                <a href="{{route('menus.items',array('menu_id'=>$menus->id))}}" class="btn-primary btn text-white" data-id="{{$menus->id}}" data-name="{{$menus->menu_name}}"><i class="fa fa-list"></i> Menu Items</a>
                                <button class="btn-primary btn btn-edit" data-id="{{$menus->id}}" data-name="{{$menus->menu_name}}"><i class="fa fa-edit"></i></button>
                                <button class="btn-danger btn btn-delete" data-id="{{$menus->id}}" data-name="{{$menus->menu_name}}"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                    @endforeach
                   
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="menu-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Menu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="menuModalForm">
                        <div class="form-group">
                            <label for="menu_name">Menu Name</label>
                            <input type="text" class="form-control" name="menu_name">
                            <input type="hidden" class="form-control" name="menu_id">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-save-changes">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="delete-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete Menu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('menus.delete')}}" method="post" id="delete-form">
                        {{ csrf_field() }}
                        <p> Are you sure you want to delete <span class="delete-item-name"></span>?</p>
                        <input type="hidden" name="menu_id_delete">
                    </form>
                </div>
                <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-delete-item">Delete</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- for custom JS and js declarations in a specific page --}}
@section('custom-js')
<script>
    $(document).ready(function(){
        $('.btn-add').click(function(){
            $('#menu-modal').modal('show');
        });

        $('.btn-edit').click(function(){
            var id = $(this).data('id');
            var name = $(this).data('name');

            $('input[name="menu_name"]').val(name);
            $('input[name="menu_id"]').val(id);
            $('.modal-title').text('Edit '+name);
            $('#menu-modal').modal('show');
            $('.btn-save-changes').attr('data-edit',1);
        });

        $('.btn-delete').click(function(){
            var id = $(this).data('id');
            var name = $(this).data('name');

            $('.delete-item-name').text(name);
            $('input[name="menu_id_delete"]').val(id);
            $('#delete-modal').modal('show');
        });

        $('.btn-delete-item').click(function(){
            $('#delete-form').submit();
        });

        $('#menu-modal').on('hide.bs.modal', function (e) {
            $('.modal-title').text('Add Menu');
            $('input[name="menu_name"]').val('');
            $('.btn-save-changes').removeAttr('data-edit');

        })

        $('.btn-save-changes').click(function(){
            var isEdit = $('.btn-save-changes').data('edit');
            var url = '{{route("menus.add")}}';
            if(isEdit){
                url = '{{route("menus.edit")}}';
            }

            $.ajax({
                url: url,
                data: {menu_name:$('input[name="menu_name"]').val(), menu_id: $('input[name="menu_id"]').val(), _token:'{{ csrf_token() }}',},
                dataType:'json',
                type: 'post',
                success:function(res){
                    if(res){
                      location.reload();  
                    }
                }
            });
        });
    });
</script>
@endsection