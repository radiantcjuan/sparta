@extends('layouts.app')

@section('content')
    {{csrf_field()}}
    <div class="container-fluid agent-list-container">
        <div class="agent-list-inner-container">
            <div class="agent-list-header my-5">
                <h1>Sales Agent Status Summary</h1>
            </div>
            <div class="row">
                @foreach ($arrAgents as $agent)
                    <div class="col-12 col-md-4 pt-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-5 profile-pic-container">
                                        <div class="profile-pic" style="background-image:url({{ ($agent['profile-pic']) ? \Storage::url($agent['profile-pic']) : 'images/profile-placeholder.png'}} );"></div>
                                    </div>
                                    <div class="col-7 d-inline-block agent-basic-info">
                                        <h4 class="sales-agent-name">{{$agent['name']}}</h4>
                                        <p><span class="sales-count">{{$agent['count']}}</span> Sales</p>
                                        <p><span class="sales-count-revenue">{{$agent['sales-revenue']}}</span> Sales Revenue</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <a href="{{route('dashboard',[ 'agent'=>strtolower( str_replace(' ','-',$agent['id']) ) ])}}" class="btn-view-profle">View Profile</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="agent-sales-count-statistics-container py-5">
            <div class="card">
                <div class="card-header">
                    <div class="float-left">
                        <i class="fa fa-line-chart pr-2"></i>
                        SALES COUNT REPORT
                    </div>
                    <div class="float-right settings dropdown">
                        <button class="btn dropdown-toggle p-0" type="button" id="ddMbSalesCount" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-gear"></i>
                        </button>
                        <div class="dropdown-menu dd-settings" aria-labelledby="ddMbSalesCount">
                            <a class="dropdown-item dd-settings-item active" href="javascript:void(0)" data-settings="weekly">Weekly</a>
                            <a class="dropdown-item dd-settings-item" href="javascript:void(0)" data-settings="monthly">Monthly</a>
                            <a class="dropdown-item dd-settings-item" href="javascript:void(0)" data-settings="quarterly">Quarterly</a>
                            <a class="dropdown-item dd-settings-item advance-settings" href="javascript:void(0)"><strong><i class="fa fa-gear"></i> Advance Settings</strong></a>
                        </div>
                    </div>
                </div>
                <div class="card-body salesCountContainer">
                    <canvas id="salesCount"></canvas>
                </div>
                <div class="card-footer">
                    
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/js/dashboard/dashboard.js"></script>
@endsection