@extends('layouts.app')

@section('content')
    <div class="users-container w-50 container-fluid">
        @include('layouts.head-title',array('title'=>'Advisory','isAjax'=>0,'url'=>route('advisory.create'),'create'=>1,'parent'=>route('advisory.index')))
        <div class="container-fluid pt-4">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            <div class="col-12">
                <form action="{{ route('advisory.store') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header">
                            Add User
                        </div>
                        <div class="card-body">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12 col-md-12">
                                        <div class=form-group>
                                            <label for="name">Title</label>
                                            <input type="text" class="form-control" name="title">
                                        </div>
                                        <div class=form-group>
                                            <label for="password">Excerpt</label>
                                            <input type="text" maxlength="100" class="form-control" name="excerpt">
                                        </div>
                                        <div class=form-group>
                                            <label for="advisoryMessage">Message</label>
                                            <textarea name="advisoryMessage" id="advisoryMessage" class="form-control"></textarea>
                                        </div>
                                        <div class=form-group>
                                            <label for="email"><input type="checkbox" name="published"> Publish</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" class="btn btn-success" value="Submit">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

{{-- for custom JS and js declarations in a specific page --}}
@section('scripts')
    <script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
    <script src="/js/admin/advisory.js"></script>
@endsection