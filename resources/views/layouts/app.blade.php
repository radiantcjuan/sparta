<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">


        <title>{{ config('app.name', 'Laravel') }}</title>
    </head>
    <body>
        @php
            $user = \Auth::user();
            $gRole = App\Role::find($user->role_id);
        @endphp
        {{-- NAVIGATION BAR --}}
        @include('layouts.nav')
        
        {{-- MAIN CONTENT --}}
        @yield('content')
        {{-- MAIN CONTENT --}}
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        @yield('scripts')
    </body>
</html>