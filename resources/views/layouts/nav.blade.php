<div class="navigation-upper-bar container-fluid">
    {{-- BURGER MENU BUTTON HERE --}}
    <a href="javascript:void(0);" class="btn-side-nav"><i class="fa fa-bars d-inline-block burger-menu"></i></a>
    {{-- NAVIGATION ITEMS --}}
    <div class="sparta-logo d-inline-block"></div>
    <div class="user-options-container d-inline-block">
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle border-none" type="button" id="profileMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="user-profile-pic" style=""></div>
            </button>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="profileMenu">
                <a class="dropdown-item" href="{{route('users.edit',['id'=>$user->_id])}}">Account Settings</a>
                <a class="dropdown-item" href="/logout">Logout</a>
            </div>
        </div>
        {{-- UL of logout and settings --}}
    </div>
</div>

@include('nav/side-nav')