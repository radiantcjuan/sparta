@php
$user = \Auth::user();
$gRole = App\Role::find($user->role_id);
@endphp
<div class="container-fluid clearfix">
    <div class="container-fluid p-3">
        <h3 class="my-0 float-left">
            @if(isset($parent))
                <a class="text-dark mr-2" href="{{ $parent }}"><i class="fa fa-arrow-left"></i></a>
            @endif
            {{$title}}
        </h3>
        @if($create && $gRole->name == 'Admin')
            <a href="{{ ($isAjax) ? 'javascript:void(0)' : $url }}" class="btn btn-success btn-add float-right"><i class="fa fa-plus"></i> Add {{$title}}</a>
        @endif
    </div>
</div>