@extends('layouts.app')
@section('content')
    {{csrf_field()}}
    <input type="hidden" name="userId" value="{{$arrAgents['id']}}">
    <div class="users-container">

        <div class="container-fluid basic-info-container">
            <div class="row">
                <div class="col-2 profile-pic-container">
                <div class="profile-pic" style="background-image:url({{ ($user->profile_pic) ? \Storage::url($user->profile_pic) :'images/profile-placeholder.png' }})"></div>
                    <div class="agent-desc">
                        <div class="agent-name">{{$user->name}}</div>
                        <div class="agent-achievement">
                            @php
                                $rankTemp = '';
                            @endphp
                            @foreach (config('constants.achievements') as $rank=>$numberOfSales)
                                @if($arrAgents['count'] >= $numberOfSales)
                                    @php
                                        $rankTemp = $rank;
                                    @endphp
                                @endif
                            @endforeach
                            {{$rankTemp}}
                        </div>
                    </div>
                </div>
                <div class="col-10 performance-dashboard-section">
                    <h2 class="dashboard-header"><i class="fa fa-home"></i> Performance Dashboard</h2>
                    <div class="row">
                        <div class="col-5 announcement">
                            @if($advisory)
                                <h3>Announcement</h3>
                                
                                <h4>{{$advisory->title}}</h4>
                                <span class="announcement-excerpt">
                                    {{$advisory->excerpt}}
                                </span>
                                <a href="javascript:void(0)" class="read-more-advisory d-block">read more</a>
                            @endif
            
                        </div>
                        <div class="col-3 total-sales align-self-start mt-auto">
                            <div class="d-inline-block count-number">
                                <span>{{$arrAgents['count']}}</span>
                            </div>
                            <div class="d-inline-block count-label">
                                <span>Total Sales</span>
                            </div>
                        </div>
                        <div class="col-4 total-revenue-per-merchant align-self-start mt-auto">
                            <div class="d-inline-block count-number">
                                <span>{{$arrAgents['sales-revenue']}}</span>
                            </div>
                            <div class="d-inline-block count-label">
                                <span>Total Revenue<br></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid report-dashboard">
            <div class="row">
                <div class="col col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="float-left">
                                <i class="fa fa-line-chart pr-2"></i>
                                SALES COUNT REPORT
                            </div>
                            <div class="float-right settings dropdown">
                                <button class="btn dropdown-toggle p-0" type="button" id="ddMbSalesCount" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-gear"></i>
                                </button>
                                <div class="dropdown-menu dd-settings" aria-labelledby="ddMbSalesCount">
                                    <a class="dropdown-item dd-settings-item active" href="javascript:void(0)" data-settings="weekly">Weekly</a>
                                    <a class="dropdown-item dd-settings-item" href="javascript:void(0)" data-settings="monthly">Monthly</a>
                                    <a class="dropdown-item dd-settings-item" href="javascript:void(0)" data-settings="quarterly">Quarterly</a>
                                    <a class="dropdown-item dd-settings-item advance-settings" href="javascript:void(0)"><strong><i class="fa fa-gear"></i> Advance Settings</strong></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body salesCountContainer">
                            <canvas id="salesCount"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4 mb-4">
                <div class="col col-12 col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <div class="float-left">
                                <i class="fa fa-pie-chart pr-2"></i>
                                SALES PER REGION
                            </div>
                            <div class="float-right settings dropdown">
                                <button class="btn dropdown-toggle p-0" type="button" id="ddFilterByCountry" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-gear"></i>
                                </button>
                                <div class="dropdown-menu dd-settings dd-filter-by-country" aria-labelledby="ddFilterByCountry">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="chart-container" style="position: relative; height:%; width:100%">
                                <canvas id="salesPerRegion"></canvas>
                            </div>
                        </div>
                        <div class="card-footer chart-legend">
                            <ul class="pt-2">
                                {{-- <li><i class="fa fa-circle" style="color: rgba(187,81,32,1)"></i> NSW</li>
                                <li><i class="fa fa-circle" style="color: rgba(125,240,130,1)"></i> QLD</li>
                                <li><i class="fa fa-circle" style="color: rgba(136,105,91,1)"></i> SA</li>
                                <li><i class="fa fa-circle" style="color: rgba(241,238,89,1)"></i> TA</li>
                                <li><i class="fa fa-circle" style="color: rgba(238,64,68,1)"></i> VIC</li>
                                <li><i class="fa fa-circle" style="color: rgba(32,129,187,1)"></i> WA</li> --}}
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <div class="float-left">
                                <i class="fa fa-info-circle pr-2"></i>
                                MERCHANT INFO
                            </div>
                            {{-- <div class="float-right settings">
                                <i class="fa fa-gear"></i>
                            </div> --}}
                        </div>
                        <div class="card-body">
                            <table class="table" id="merchantInfoReport"">
                                <thead>
                                    <tr>
                                        <th scope="col">Merchant Name</th>
                                        <th scope="col">Phone Number</th>
                                        <th scope="col">Total Revenue</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            <div class="float-right">
                                <a href="javascript:void(0)" class="merchant-info-view-more">View More <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid other-sales-personel mx-auto mb-5">
            <div class="card">
                <div class="card-header">
                    Collegues Sales Count
                </div>
                <div class="card-body">
                    <ul class="other-agents-list-group">
                        @foreach($agents as $agent=>$salesCount)
                            <li class="other-agents-list-item">
                                <div class="container-fluid">
                                    <div class="other-agents-profile-pic" style="background-image:url({{ \Storage::url($salesCount['profile-pic']) }});"></div>
                                    <p class="mb-0 agent-name">{{$agent}}</p>
                                    <p class="agent-count">{{$salesCount['count']}}</p>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

@include('dashboard.modal')
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script src="/js/dashboard/sales_count.js"></script>
    <script src="/js/dashboard/sales_per_region.js"></script>
    <script src="/js/dashboard/merchant_info.js"></script>
    <script>
        $(document).ready(function(){
            $('.read-more-advisory').click(function(){
                $('#advisoryModal').modal('show');
            });
        });
    </script>
@endsection