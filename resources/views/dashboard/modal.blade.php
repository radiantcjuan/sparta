<!-- Modal -->
<div class="modal fade" id="merchinInfoViewMore" tabindex="-1" role="dialog" aria-labelledby="merchinInfoViewMoreLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="merchinInfoViewMoreLabel">Merchants You Got!</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <table class="table table-dark" id="merchantInfoReportModal">
                    <thead>
                        <tr>
                            <th scope="col">Merchant Name</th>
                            <th scope="col">Email Address</th>
                            <th scope="col">Phone Number</th>
                            <th scope="col">First Sale Date</th>
                            <th scope="col">Total Revenue</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="advanceSettings" tabindex="-1" role="dialog" aria-labelledby="advanceSettingsLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="advanceSettingsLabel">Merchants You Got!</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="start_date">Start Date</label>
                    <input type="text" name="start_date" class="form-control">
                </div>
                <div class="form-group">
                    <label for="start_date">End Date</label>
                    <input type="text" name="end_date" class="form-control">
                </div>

                <div class="form-check">
                    <input class="form-check-input" type="radio" name="customFilterType[]" id="customMonthly" value="monthly" checked>
                    <label class="form-check-label" for="customMonthly">
                        Monthly
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="customFilterType[]" id="customWeekly" value="weekly">
                    <label class="form-check-label" for="customWeekly">
                        Weekly
                    </label>
                </div>
                <div class="form-check ">
                    <input class="form-check-input" type="radio" name="customFilterType[]" id="customDaily" value="Daily">
                    <label class="form-check-label" for="customDaily">
                        Daily
                    </label>
                </div>
                {{-- <div class="form-check disabled">
                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3" disabled>
                    <label class="form-check-label" for="exampleRadios3">
                        
                    </label>
                </div> --}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-apply disabled" disabled>Apply</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@if($advisory)
<!-- Modal -->
<div class="modal fade" id="advisoryModal" tabindex="-1" role="dialog" aria-labelledby="advisoryModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="advisoryModalLabel">Advisory</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <h4>
                    {{$advisory->title}}
                        
                    </h4>
                    {!!$advisory->advisoryMessage!!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endif
