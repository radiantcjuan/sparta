@extends('layouts.app')

@section('content')
	<div class="container-fluid main-container">
	    <div class="row h-100">
	        <div class="col-2 side-nav px-0">
	            <ul class="nav flex-column nav-pills col-2 p-0 h-100 bg-white" id="v-pills-tab" role="tablist" aria-orientation="vertical">
	                @include('nav.side-nav')
	            </ul>
	        </div>
	        <div class="col-10 px-0">
	            @yield('cms-content')
	        </div>
	    </div>
	</div>
@endsection