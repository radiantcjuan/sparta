@extends('layouts.app')
@section('content')
    {{csrf_field()}}
    <div class="leaderboard-container container-fluid pt-5 pb-5">
        <div class="leaderboard-header position-relative pt-5 pb-5">
            <img class="crown" src="/images/crowns.svg" alt=""><h1>Leaderboard {{$currentQuarter['quarter']}}</h1><img class="crown2" src="/images/crowns.svg" alt="">
        </div>
        <div class="leaderboard-buttons mb-5">
            <div class="btn-group btn-group-toggle">
                <a class="btn btn-secondary {{($type == 'sales') ? 'active' : ''}}" href="{{route('leaderboard',['type'=>'sales'])}}">Sales</a>
                <a class="btn btn-secondary {{($type == 'revenue') ? 'active' : ''}}" href="{{route('leaderboard',['type'=>'revenue'])}}">Revenue</a>
            </div>
        </div>
        <table class="table table-striped">
            <thead>
                <tr class="table-header">
                    <th>RANK</th>
                    <th>AGENT</th>
                    <th>{{($type == 'sales') ? 'SALES COUNT' : 'SALES REVENUE'}}</th>
                </tr>
            </thead>
            <tbody>
                @php $rem = 3; $rank = 1; @endphp
                @foreach($collectionAgentsSorted as $agents)
                    <tr>
                        <th>
                            <span style="font-size:{{($rem != 0) ? $rem : 1}}rem !important;">{{$rank}}</span>
                            <div class="profile-pic ml-5" style="background-image:url({{ \Storage::url($agents['profile-pic']) }})"></div>
                        </th>
                        <td>{{$agents['agent']}}</td>
                        <td>{{$agents['count']}}</td>
                    </tr>
                @php $rem--; $rank++;  @endphp
                @endforeach
                
            </tbody>
        </table>
    </div>
@endsection
@section('scripts')
@endsection