<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">


        <title>{{ config('app.name', 'Laravel') }}</title>
    </head>
    <body>
        
        {{-- MAIN CONTENT --}}
        {{csrf_field()}}
        <input name="roundtype" type="hidden" value="{{$round}}">
        <div class="container-fluid roulette-container-outer pt-5">
            <div class="row">
                <div class="col-12">
                    <div class="sparta-logo d-block mb-5"></div>
                </div>
            </div>

            <div class="row mb-5 justify-content-center rounds">
                <div class="col-12">
                    <div class="btn-group btn-group-toggle">
                        <a class="btn btn-secondary {{($round == 'round1') ? 'active' : ''}}" href="{{route('roulette',['round'=>'round1'])}}">Round 1</a>
                        <a class="btn btn-secondary {{($round == 'round2') ? 'active' : ''}}" href="{{route('roulette',['round'=>'round2'])}}">Round 2</a>
                    </div>
                </div>
            </div>

            <div class="row mb-5">
                <div class="col-8">
                    <div class="roulette_container">
                        <div class="roulette" style="display:none;">
                            @foreach($collectionAgentsSorted as $agent=>$salesCount)
                                <img src="{{\Storage::url($salesCount['profile-pic'])}}"/>
                            @endforeach
                        </div>
                        <div class="background-white"></div>
                    </div>
                    <div class="btn_container">
                        <p>
                            <button class="start"></button>
                            {{-- <button class="stop btn-large btn btn-warning"> STOP </button> --}}
                        </p>

                        <audio id="lever-sound">
                            <source src="/uploads/level-pull-sound.mp3" type="audio/mpeg">
                        </audio>
                        <audio id="spinning-sound">
                            <source src="/uploads/wheel.mp3" type="audio/mpeg">
                        </audio>
                    </div>
                </div>
                <div class="col-4 rules">
                    <h4>SPARTAN ROULETTE</h4>
                    <ul class="list-group">
                        <li class="list-group-item">The person who will be chosen via spinning the Roulette will win a price sponsored by Ordermate</li>
                        <li class="list-group-item">Who ever has highest sales {{($round == 'round2') ? 'revenue' : ''}} in this Quarter will have the higher percentage(%) of winning (please see the top FYR).</li>
                        <li class="list-group-item">Congratulate the winner and hold no grudges.</li>
                        <li class="list-group-item">Have Fun!</li>
                    </ul>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-12">
                    <div class="agent-chance-sales-count">
                        <div class="card">
                            <div class="card-header">
                                Sales Percentage Of Winning
                            </div>
                            <div class="card-body">
                                <div class="agent-percentage container-fluid">
                                    @php $count = 0; @endphp
                                    @foreach($collectionAgentsSorted as $order=>$salesCount)
                                        
                                        @if($count == 0)
                                            <div class="row my-4 justify-content-center">
                                        @endif
                                                <div class="col-3 agent-percentage-item">
                                                    <div class="agent-percentage-item-inner">
                                                        <div class="profile-pic">
                                                            <div class="other-agents-profile-pic" style="background-image:url({{\Storage::url($salesCount['profile-pic'])}});"></div>
                                                        </div>
                                                        <div class="percentage-container">
                                                            <span class="agent-name">{{$salesCount['agent']}}</span><br>
                                                            <span>Sales {{($round == 'round2') ? 'revenue' : ''}}: {{$salesCount['count']}}</span>
                                                            @php

                                                            @endphp
                                                            <p>Winning chance: {{round(($salesCount['count']/$countSum)*100)}}%</p>
                                                        </div>
                                                    </div>
                                                </div>
                                        @php
                                            $count++;
                                        @endphp
                                        @if($count == 4)
                                            </div>
                                            @php  $count=0; @endphp
                                        @endif
                                       
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- MAIN CONTENT --}}
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/roulette/roulette.js') }}"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    </body>
</html>