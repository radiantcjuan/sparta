<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <title>{{ config('app.name', 'Laravel') }}</title>
    </head>
    <body>
        <div class="container-fluid h-100 d-flex w-100">
            <div class="row justify-content-center align-self-center w-100">
                <div class="col-4">
                    {{-- ORDEMATE LOGO --}}
                    <div class="ordermate-logo mx-auto"></div>
                    {{-- ORDEMATE LOGO --}}
                    
                    <div class="card login-form-container">
                        <div class="card-body login-form">
                            {{-- SPARTA LOGO --}}
                            <div class="sparta-logo mx-auto"></div>
                            {{-- SPARTA LOGO --}}
                            <form method="POST" action="login-user" class="login-form-group">
                                {{ csrf_field() }}

                                @if($errors->any())
                                    <div class="alert alert-danger">
                                        {{$errors->first()}}
                                    </div>
                                @endif
                                
                                <div class="form-group">
                                    <input type="text" class="form-control login-form-items" id="username" name="username" aria-describedby="emailHelp" placeholder="Enter email">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control login-form-items" id="password" name="password" placeholder="Password">
                                </div>
                                <div class="form-group" style="text-align:center; padding-top:25px;">
                                    <button type="submit" class="btn btn-primary btn-login">LOGIN</button>
                                </div>
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('js/app.js') }}"></script>      
    </body>
</html>