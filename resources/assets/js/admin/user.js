$(document).ready(function(){

    $('.btn-delete').click(function(){
        var id = $(this).data('id');
        var name = $(this).data('name');

        $('.delete-item-name').text(name);
        $('input[name="menu_id_delete"]').val(id);
        $('#delete-modal').modal('show');
    });

    $('.btn-delete-item').click(function(){
        $('#delete-form').submit();
    });
});