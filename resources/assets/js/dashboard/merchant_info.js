$(document).ready(function(){
    var userId = $('input[name="userId"]').val();

    $('#merchantInfoReport').DataTable({
        "lengthMenu": false,
        "paging":false,
        "searching":false,
        "info" : false,
        "ajax": "/api/dashboard/merchant-info/not-advance/"+userId,
        "columns": [
            { "data": "title" },
            { "data": "mobile_number" },
            { "data": "overall_sales_revnue" },
        ]
    });

    $('#merchantInfoReportModal').DataTable({
        "ajax": "/api/dashboard/merchant-info/advance/"+userId,
        "columns": [
            { "data": "title" },
            { "data": "email_address" },
            { "data": "mobile_number" },
            { 
                "data": "first_sale_date",
                "render": function(data,type)
                {
                    if (type == 'sort' || type == 'type')
                    {
                        return data;
                    }
                    return moment.unix(data).format('DD MMM YYYY')
                }
            },
            { "data": "overall_sales_revnue" }
        ]
    });


    $('.merchant-info-view-more').click(function(){
        $('#merchinInfoViewMore').modal('show');
    });
});
