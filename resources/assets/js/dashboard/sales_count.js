$(document).ready(function(){
    // SALES COUNT
    var defaultSalesCountSettings = 'weekly';
    var csrfToken = $('input[name="_token"]').val();
    var userId = $('input[name="userId"]').val();

    $.ajax({
        url:'/api/dashboard/sales-count/'+defaultSalesCountSettings,
        type:'POST',
        data:{user_id:userId},
        headers: {
            'X-CSRF-TOKEN': csrfToken
        },
        success:function(res)
        {

            // console.log(res);
            var label = []
            var values = [];
            for(var result in res)
            {
                label.push(result);
                values.push(res[result]);
            }
             

            
            var salesCount = document.getElementById('salesCount').getContext('2d');
            var chart = new Chart(salesCount, {
                // The type of chart we want to create
                type: 'line',

                // The data for our dataset
                data: {
                    labels: label,
                    datasets: [{
                        label: "Sales",
                        backgroundColor: 'rgba(238,64,68,0.7)',
                        borderColor: '#ee4044',
                        data: values,
                    }]
                },
                options: {
                    legend: {
                        display: false,
                    }
                }
            });
        },
        error:function(err)
        {
            console.log(err);
        }

    });

    $('.dd-settings-item').click(function(){
        if($(this).attr('class') != "dropdown-item dd-settings-item advance-settings")
        {
            if($(this).attr('class') != "dropdown-item dd-settings-item advance-settings active")
            {
                var defaultSalesCountSettings = $(this).data('settings');
                $('.dd-settings').find('.dd-settings-item').removeClass('active');
                $(this).addClass('active');
                $.ajax({
                    url:'/api/dashboard/sales-count/'+defaultSalesCountSettings,
                    type:'POST',
                    data:{user_id:userId},
                    headers: {
                        'X-CSRF-TOKEN': csrfToken
                    },
                    success:function(res)
                    {
                        $('#salesCount').remove(); // this is my <canvas> element
                        $('.salesCountContainer').append('<canvas id="salesCount"><canvas>');

                        var label = []
                        var values = [];
                        for(var result in res)
                        {
                            label.push(result);
                            values.push(res[result]);
                        }
                                    
                        var salesCount = document.getElementById('salesCount').getContext('2d');
                        var chart = new Chart(salesCount, {
                            // The type of chart we want to create
                            type: 'line',
                            // The data for our dataset
                            data: {
                                labels: label,
                                datasets: [{
                                    label: "Sales",
                                    backgroundColor: 'rgba(238,64,68,0.7)',
                                    borderColor: '#ee4044',
                                    data: values,
                                }]
                            },
                            options: {
                                legend: {
                                    display: false,
                                }
                            }
                        });
                    },
                    error:function(err)
                    {
                        console.log(err);
                    }

                }); 
            }
        }
        
    });

    $('.advance-settings').click(function(){
        $('#advanceSettings').modal('show');
    });

    $('input[name="start_date"]').datepicker({
        uiLibrary: 'bootstrap4'
    });

    $('input[name="end_date"]').datepicker({
        uiLibrary: 'bootstrap4'
    });

    //datepicker events

    $('input[name="start_date"]').change(function(){
        if($('input[name="end_date"]').val() != '')
        {
            $('.btn-apply').removeClass('disabled');
            $('.btn-apply').removeAttr('disabled');
        }
        else
        {
            $('.btn-apply').addClass('disabled');
            $('.btn-apply').attr('disabled');
        }
    });

    $('input[name="end_date"]').change(function(){
        var dateStartDate = new Date($('input[name="start_date"]').val());
        var epochStartDate = dateStartDate.getTime();

        var dateEndDate = new Date($(this).val()); // some mock date
        var epochEndDate = dateEndDate.getTime();
        
        var diff = Math.abs(epochStartDate - epochEndDate);

        if((2.592e+9) > diff)
        {
            $('#customMonthly').parent().addClass('disabled');
            $('#customMonthly').attr('disabled','disabled');
            if((6.048e+8) > diff)
            {
                $('#customWeekly').parent().addClass('disabled');
                $('#customWeekly').attr('disabled','disabled');
                $('#customDaily').attr('checked','checked');
            } else if((6.048e+8) <= diff) {
                $('#customWeekly').parent().removeClass('disabled');
                $('#customWeekly').attr('disabled',false);
            }
        } else if((2.592e+9) <= diff) {
            $('#customMonthly').parent().removeClass('disabled');
            $('#customMonthly').attr('disabled',false);
        }

        if($('input[name="start_date"]').val() != '')
        {
            $('.btn-apply').removeClass('disabled');
            $('.btn-apply').removeAttr('disabled');
        }
        else
        {
            $('.btn-apply').addClass('disabled');
            $('.btn-apply').attr('disabled');
        }
    });

    $('.btn-apply').click(function(){
        var defaultSalesCountSettings = $('input[name="customFilterType[]"]:checked').val();
        console.log(defaultSalesCountSettings);
        $('.dd-settings').find('.dd-settings-item').removeClass('active');
        $('.advance-settings').addClass('active');

        var strStartDate = $('input[name="start_date"]').val();
        var strEndDate = $('input[name="end_date"]').val();

        $.ajax({
            url:'/api/dashboard/sales-count/'+defaultSalesCountSettings,
            type:'POST',
            data: {startDate: strStartDate, endDate:strEndDate,user_id:userId},
            headers: {
                'X-CSRF-TOKEN': csrfToken
            },
            success:function(res)
            {

                $('#salesCount').remove(); // this is my <canvas> element
                $('.salesCountContainer').append('<canvas id="salesCount"><canvas>');

                var label = []
                var values = [];
                for(var result in res)
                {
                    label.push(result);
                    values.push(res[result]);
                }
                console.log(label);       
                var salesCount = document.getElementById('salesCount').getContext('2d');
                var chart = new Chart(salesCount, {
                    // The type of chart we want to create
                    type: 'line',
                    // The data for our dataset
                    data: {
                        labels: label,
                        datasets: [{
                            label: "Sales",
                            backgroundColor: 'rgba(238,64,68,0.7)',
                            borderColor: '#ee4044',
                            data: values,
                        }]
                    },
                    options: {
                        legend: {
                            display: false,
                        }
                    }
                });
                
                $('#advanceSettings').modal('hide');

            },
            error:function(err)
            {
                console.log(err);
            }
    
        }); 
    });
});