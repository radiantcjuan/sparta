$(document).ready(function(){
    var csrfToken = $('input[name="_token"]').val();
    var userId = $('input[name="userId"]').val();
    var defaultCountry = "AU";

    function random_rgba(hover = false) {
        var o = Math.round, r = Math.random, s = 255;
        var oppa = (!hover) ? 0.5 : 1;
        return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + oppa + ')';
    }

    $.ajax({
        url:'/api/dashboard/sales-per-region',
        type:'POST',
        data:{user_id:userId,country:defaultCountry},
        headers: {
            'X-CSRF-TOKEN': csrfToken
        },
        success:function(res)
        {
                    // PIE CHART
            console.log(res['intAdministrativeAreaCount']);
            var label = [];
            var value = [];
            var colorArr = [];
            var colorArrhOVER = [];

            for(var adArea in res['intAdministrativeAreaCount'])
            {
                var color = random_rgba();
                label.push(res['intAdministrativeAreaCount'][adArea].administrative_area);
                value.push(res['intAdministrativeAreaCount'][adArea].count);
                colorArr.push(color);
                colorArrhOVER.push('#ee4044');
                $('.chart-legend .pt-2').append('<li class="px-1"><i class="fa fa-circle" style="color: '+color+'"></i> '+res['intAdministrativeAreaCount'][adArea].administrative_area+'</li>');
            }

            for(var keyCoinutry in res['countries'])
            {
                $('.dd-filter-by-country').append('<a class="dropdown-item dd-filter-by-country-item '+ ((defaultCountry == res['countries'][keyCoinutry]) ? 'active' : '') +'" href="javascript:void(0)" data-settings="'+res['countries'][keyCoinutry]+'">'+res['countries'][keyCoinutry]+'</a>')
            }

            var pie = document.getElementById('salesPerRegion').getContext('2d');
            var data = {
                datasets: [{
                    data: value,
                    label: "Sales Per Region",
                    backgroundColor: colorArr,
                    borderColor: 'rgba(255,255,255,1)',
                    borderWidth: 0,
                    hoverBackgroundColor: colorArrhOVER,
                    hoverBorderColor: 'rgba(255,255,255,1)',
                    hoverBorderWidth: 1,
                }],

                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels:label,
            };

            var myPieChart = new Chart(pie,{
                type: 'pie',
                data: data,
                maintainAspectRatio: false,

                options: {
                    legend: {
                        display: false,
                    }
                }
            });
        },
        error:function(err)
        {
            console.log(err);
        }

    });

    
    $(document).on('click','.dd-filter-by-country-item',function(){
        defaultCountry = $(this).data('settings');
        $.ajax({
            url:'/api/dashboard/sales-per-region',
            type:'POST',
            data:{user_id:userId,country:defaultCountry},
            headers: {
                'X-CSRF-TOKEN': csrfToken
            },
            success:function(res)
            {
                        // PIE CHART
                console.log(res['intAdministrativeAreaCount']);
                var label = [];
                var value = [];
                var colorArr = [];
                var colorArrhOVER = [];
                $('.chart-legend .pt-2').html('');
                for(var adArea in res['intAdministrativeAreaCount'])
                {
                    var color = random_rgba();
                    label.push(res['intAdministrativeAreaCount'][adArea].administrative_area);
                    value.push(res['intAdministrativeAreaCount'][adArea].count);
                    colorArr.push(color);
                    colorArrhOVER.push('#ee4044');
                    $('.chart-legend .pt-2').append('<li class="px-1"><i class="fa fa-circle" style="color: '+color+'"></i> '+res['intAdministrativeAreaCount'][adArea].administrative_area+'</li>');
                }

                $('.dd-filter-by-country').html('');
                for(var keyCoinutry in res['countries'])
                {
                    $('.dd-filter-by-country').append('<a class="dropdown-item dd-filter-by-country-item '+ ((defaultCountry == res['countries'][keyCoinutry]) ? 'active' : '') +'" href="javascript:void(0)" data-settings="'+res['countries'][keyCoinutry]+'">'+res['countries'][keyCoinutry]+'</a>')
                }

                $('#salesPerRegion').remove();
                $('.chart-container').append('<canvas id="salesPerRegion"></canvas>');
    
                var pie = document.getElementById('salesPerRegion').getContext('2d');
                var data = {
                    datasets: [{
                        data: value,
                        label: "Sales Per Region",
                        backgroundColor: colorArr,
                        borderColor: 'rgba(255,255,255,1)',
                        borderWidth: 0,
                        hoverBackgroundColor: colorArrhOVER,
                        hoverBorderColor: 'rgba(255,255,255,1)',
                        hoverBorderWidth: 1,
                    }],
    
                    // These labels appear in the legend and in the tooltips when hovering different arcs
                    labels:label,
                };
    
                var myPieChart = new Chart(pie,{
                    type: 'pie',
                    data: data,
                    maintainAspectRatio: false,
    
                    options: {
                        legend: {
                            display: false,
                        }
                    }
                });
            },
            error:function(err)
            {
                console.log(err);
            }
    
        });
    });

});