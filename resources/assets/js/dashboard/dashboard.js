$(document).ready(function(){
    //DATA TABLE MERCHANT INFO SECTION
    var defaultSalesCountSettings = 'weekly';
    var csrfToken = $('input[name="_token"]').val();
    var userId = $('input[name="userId"]').val();
    
    function random_rgba(hover = false) {
        var o = Math.round, r = Math.random, s = 255;
        var oppa = 1;
        return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + oppa + ')';
    }

    $.ajax({
        url:'/api/dashboard/sales-count-admin/'+defaultSalesCountSettings,
        type:'POST',
        data:{user_id:userId},
        headers: {
            'X-CSRF-TOKEN': csrfToken
        },
        success:function(res)
        {

            // console.log(res);
  
            var datasets = [];
            for(var result in res)
            {
                var label = []
                var values = [];
                for(var xAxisLabel in res[result].count)
                {
                    label.push(xAxisLabel);
                    values.push(res[result].count[xAxisLabel]);
                }
                datasets.push(
                    {
                        label: res[result].user,
                        borderColor: random_rgba(),
                        lineTension: 0,
                        data: values,
                    }
                )
                
                values.push(res[result]);
            }
             
            var salesCount = document.getElementById('salesCount').getContext('2d');
            var chart = new Chart(salesCount, {
                // The type of chart we want to create
                type: 'line',

                // The data for our dataset
                data: {
                    labels: label,
                    datasets: datasets
                },
                options: {
                    legend: {
                        display: true,
                    }
                }
            });
        },
        error:function(err)
        {
            console.log(err);
        }

    });


    
    $('.dd-settings-item').click(function(){
        if($(this).attr('class') != "dropdown-item dd-settings-item advance-settings")
        {
            if($(this).attr('class') != "dropdown-item dd-settings-item advance-settings active")
            {
                var defaultSalesCountSettings = $(this).data('settings');
                $('.dd-settings').find('.dd-settings-item').removeClass('active');
                $(this).addClass('active');
                $.ajax({
                    url:'/api/dashboard/sales-count-admin/'+defaultSalesCountSettings,
                    type:'POST',
                    data:{user_id:userId},
                    headers: {
                        'X-CSRF-TOKEN': csrfToken
                    },
                    success:function(res)
                    {
                        $('#salesCount').remove(); // this is my <canvas> element
                        $('.salesCountContainer').append('<canvas id="salesCount"><canvas>');

                        var datasets = [];
                        for(var result in res)
                        {
                            var label = []
                            var values = [];
                            for(var xAxisLabel in res[result].count)
                            {
                                label.push(xAxisLabel);
                                values.push(res[result].count[xAxisLabel]);
                            }
                            datasets.push(
                                {
                                    label: res[result].user,
                                    borderColor: random_rgba(),
                                    lineTension: 0,
                                    data: values,
                                }
                            )
                            
                            values.push(res[result]);
                        }
                        
                        var salesCount = document.getElementById('salesCount').getContext('2d');
                        var chart = new Chart(salesCount, {
                            // The type of chart we want to create
                            type: 'line',

                            // The data for our dataset
                            data: {
                                labels: label,
                                datasets: datasets
                            },
                            options: {
                                legend: {
                                    display: true,
                                }
                            }
                        });
                    },
                    error:function(err)
                    {
                        console.log(err);
                    }

                }); 
            }
        }
        
    });

    $('.advance-settings').click(function(){
        $('#advanceSettings').modal('show');
    });

    $('input[name="start_date"]').datepicker({
        uiLibrary: 'bootstrap4'
    });

    $('input[name="end_date"]').datepicker({
        uiLibrary: 'bootstrap4'
    });


    $('input[name="start_date"]').change(function(){
        if($('input[name="end_date"]').val() != '')
        {
            $('.btn-apply').removeClass('disabled');
            $('.btn-apply').removeAttr('disabled');
        }
        else
        {
            $('.btn-apply').addClass('disabled');
            $('.btn-apply').attr('disabled');
        }
    });

    $('input[name="end_date"]').change(function(){
        var dateStartDate = new Date($('input[name="start_date"]').val());
        var epochStartDate = dateStartDate.getTime();

        var dateEndDate = new Date($(this).val()); // some mock date
        var epochEndDate = dateEndDate.getTime();
        
        var diff = Math.abs(epochStartDate - epochEndDate);

        if((2.592e+9) > diff)
        {
            $('#customMonthly').parent().addClass('disabled');
            $('#customMonthly').attr('disabled','disabled');
            if((6.048e+8) > diff)
            {
                $('#customWeekly').parent().addClass('disabled');
                $('#customWeekly').attr('disabled','disabled');
                $('#customDaily').attr('checked','checked');
            } else if((6.048e+8) <= diff) {
                $('#customWeekly').parent().removeClass('disabled');
                $('#customWeekly').attr('disabled',false);
            }
        } else if((2.592e+9) <= diff) {
            $('#customMonthly').parent().removeClass('disabled');
            $('#customMonthly').attr('disabled',false);
        }

        if($('input[name="start_date"]').val() != '')
        {
            $('.btn-apply').removeClass('disabled');
            $('.btn-apply').removeAttr('disabled');
        }
        else
        {
            $('.btn-apply').addClass('disabled');
            $('.btn-apply').attr('disabled');
        }
    });

    $('.btn-apply').click(function(){
        var defaultSalesCountSettings = $('input[name="customFilterType[]"]:checked').val();
        console.log(defaultSalesCountSettings);
        $('.dd-settings').find('.dd-settings-item').removeClass('active');
        $('.advance-settings').addClass('active');

        var strStartDate = $('input[name="start_date"]').val();
        var strEndDate = $('input[name="end_date"]').val();

        $.ajax({
            url:'/api/dashboard/sales-count-admin/'+defaultSalesCountSettings,
            type:'POST',
            data: {startDate: strStartDate, endDate:strEndDate},
            headers: {
                'X-CSRF-TOKEN': csrfToken
            },
            success:function(res)
            {

                $('#salesCount').remove(); // this is my <canvas> element
                $('.salesCountContainer').append('<canvas id="salesCount"><canvas>');

                var datasets = [];
                for(var result in res)
                {
                    var label = []
                    var values = [];
                    for(var xAxisLabel in res[result].count)
                    {
                        label.push(xAxisLabel);
                        values.push(res[result].count[xAxisLabel]);
                    }
                    datasets.push(
                        {
                            label: res[result].user,
                            borderColor: random_rgba(),
                            lineTension: 0,
                            data: values,
                        }
                    )
                    
                    values.push(res[result]);
                }
                
                var salesCount = document.getElementById('salesCount').getContext('2d');
                var chart = new Chart(salesCount, {
                    // The type of chart we want to create
                    type: 'line',

                    // The data for our dataset
                    data: {
                        labels: label,
                        datasets: datasets
                    },
                    options: {
                        legend: {
                            display: true,
                        }
                    }
                });
                
                $('#advanceSettings').modal('hide');

            },
            error:function(err)
            {
                console.log(err);
            }
    
        }); 
    });
}); 