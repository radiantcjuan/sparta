<?php

/* Put all the constant value here */

return [
	'admin_link' => "admin-".env('APP_ENV'),
	'achievements'=>[
		'Warrior' => 30,
		'Master' => 60,
		'Grand Master' => 100,
		'Grand Master 2' => 130,
		'Epic' => 170,
		'Grand Epic' => 200,
		'Mythic' => 500
	]
];