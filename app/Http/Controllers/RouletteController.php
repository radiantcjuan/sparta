<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MerchantInfoImport;
use App\User;
use App\Winner;

class RouletteController extends Controller
{
    public function index(Request $request)
    {
        $this->isAdmin();
        $objUsersList = User::with(['role'=>function($role){
            return $role->where('name','!=','Admin');
        }])->where('status','=',1)->get();

        $agents = [];
        $countSum = 0;
        $round = $request->round;

        // quaters
        $currentQuarter = $this->get_current_quarter();
        foreach($objUsersList as $userList)
        {
            if($userList->role)
            {

                $merchantInfoReportAgents = \App\MerchantInfoImport::where('SALESPERSON','like','%'.$userList->name.'%')->where('FIRST SALE DATE','>=',$currentQuarter['start_date'])->where('FIRST SALE DATE','<=',$currentQuarter['end_date'])->groupBy('TITLE');

                if($round == 'round1')
                {
                    $merchantInfoReportAgents = $merchantInfoReportAgents->get(['TITLE','SALESPERSON','OVERALL SALES REVENUE']);
                }
                else
                {
                    $merchantInfoReportAgents = $merchantInfoReportAgents->get(['TITLE','SALESPERSON','OVERALL SALES REVENUE']);
                    $merchantInfoReportAgentsSum = $merchantInfoReportAgents->sum('OVERALL SALES REVENUE');
                }
                                

                $agents[] = array(
                    'agent' => $userList->name,
                    'count' =>  ($round == 'round1') ? $merchantInfoReportAgents->count() : $merchantInfoReportAgentsSum,
                    'profile-pic' => $userList->profile_pic
                );


                $countSum += ($round == 'round1') ? $merchantInfoReportAgents->count() : $merchantInfoReportAgentsSum;
            }
        }

        $collectionAgents = collect($agents);
        $collectionAgentsSorted = $collectionAgents->sortByDesc('count');

        return view('roulette.index',compact('collectionAgentsSorted','countSum','round'));
    }

    private function get_current_quarter()
    {
        $quarters = [
            [
                'quarter' => 'Q1',
                'start_date' => strtotime('july 1'),
                'end_date' => strtotime('september 30')
            ],

            [
                'quarter' => 'Q2',
                'start_date' => strtotime('october 1'),
                'end_date' => strtotime('december 31')
            ],

            [
                'quarter' => 'Q3',
                'start_date' => strtotime('january 1'),
                'end_date' => strtotime('march 31')
            ],

            [
                'quarter' => 'Q4',
                'start_date' => strtotime('april 1'),
                'end_date' => strtotime('june 30')
            ]
        ];
        
        $quarter = '';
        
        foreach($quarters as $q)
        {
            if($q['start_date'] <= strtotime('now') && $q['end_date'] >= strtotime('now'))
            {
                $quarter = $q;
            }
        }

        return $quarter;
    }

    public function get_winner(Request $request)
    {
        $objUsersList = User::with(['role'=>function($role){
            return $role->where('name','!=','Admin');
        }])->where('status','=',1)->get();
        $agents = [];
        $countSum = 0;
        $round = $request->round;

        foreach($objUsersList as $userList)
        {
            if($userList->role)
            {
                $merchantInfoReportAgents = \App\MerchantInfoImport::where('SALESPERSON','like','%'.$userList->name.'%')->groupBy('TITLE');

                if($round == 'round1')
                {
                    $merchantInfoReportAgents = $merchantInfoReportAgents->get(['TITLE','SALESPERSON','OVERALL SALES REVENUE']);
                }
                else
                {
                    $merchantInfoReportAgents = $merchantInfoReportAgents->get(['TITLE','SALESPERSON','OVERALL SALES REVENUE']);
                    $merchantInfoReportAgentsSum = $merchantInfoReportAgents->sum('OVERALL SALES REVENUE');
                }

                $agents[] = array(
                    'count' => ($round == 'round1') ? $merchantInfoReportAgents->count() : $merchantInfoReportAgentsSum,
                    'agent_name' => $userList->name
                );
                
                $countSum += ($round == 'round1') ? $merchantInfoReportAgents->count() : $merchantInfoReportAgentsSum;
            }
        }
        
        $collectionAgents = collect($agents);
        $collectionAgentsSorted = $collectionAgents->sortByDesc('count');
   

        $winnerList = [];
        foreach($collectionAgents as $order=>$salesCount)
        {
            $winner = rand(round(($salesCount['count']/$countSum)*100),100);
            $winnerList[] = array(
                'agent' => $order,
                'agent_name'=>$salesCount['agent_name'],
                'winner' => $winner,
            );
        }

        $collectionAgents = collect($winnerList);
        $collectionAgentsSorted = $collectionAgents->sortByDesc('winner');

        return $collectionAgentsSorted->first();
    }

    public function register_winner(Request $request)
    {
        
        $agent = $request->agent;
        $userWinner = User::where('name',$agent)->first();

        $winner = new Winner;
        $winner->user_id = $userWinner->_id;
        $winner->round = $request->round;
        $winner->date_created = date('Y-m-d');
        $winner->price_won = '';
        $winner->quarter = '';  
        $winner->save(); 
        
        return "true";
    }
}
