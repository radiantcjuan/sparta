<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MerchantInfoImport;
use App\User;

class LeaderBoardController extends Controller
{
    //
    public function index(Request $request)
    {

        $objUsersList = User::with(['role'=>function($role){
            return $role->where('name','!=','Admin');
        }])->where('status','=',1)->get();

        $agents = [];
        $countSum = 0;
        $type = $request->type;

        // quaters
    $currentQuarter = $this->get_current_quarter();
        foreach($objUsersList as $userList)
        {
            if($userList->role)
            {

                $merchantInfoReportAgents = \App\MerchantInfoImport::where('SALESPERSON','like','%'.$userList->name.'%')->where('FIRST SALE DATE','>=',$currentQuarter['start_date'])->where('FIRST SALE DATE','<=',$currentQuarter['end_date'])->groupBy('TITLE');

                if($type == 'sales')
                {
                    $merchantInfoReportAgents = $merchantInfoReportAgents->get(['TITLE','SALESPERSON','OVERALL SALES REVENUE']);
                }
                else
                {
                    $merchantInfoReportAgents = $merchantInfoReportAgents->get(['TITLE','SALESPERSON','OVERALL SALES REVENUE']);
                    $merchantInfoReportAgentsSum = $merchantInfoReportAgents->sum('OVERALL SALES REVENUE');
                }
                                

                $agents[] = array(
                    'agent' => $userList->name,
                    'count' =>  ($type == 'sales') ? $merchantInfoReportAgents->count() : $merchantInfoReportAgentsSum,
                    'profile-pic' => $userList->profile_pic
                );
            }
        }

        $collectionAgents = collect($agents);
        $collectionAgentsSorted = $collectionAgents->sortByDesc('count');

        return view('leaderboard.index',compact('collectionAgentsSorted','type','currentQuarter'));
    }

    private function get_current_quarter()
    {
        $quarters = [
            [
                'quarter' => 'Q1',
                'start_date' => strtotime('july 1'),
                'end_date' => strtotime('september 30')
            ],

            [
                'quarter' => 'Q2',
                'start_date' => strtotime('october 1'),
                'end_date' => strtotime('december 31')
            ],

            [
                'quarter' => 'Q3',
                'start_date' => strtotime('january 1'),
                'end_date' => strtotime('march 31')
            ],

            [
                'quarter' => 'Q4',
                'start_date' => strtotime('april 1'),
                'end_date' => strtotime('june 30')
            ]
        ];
        
        $quarter = '';
        
        foreach($quarters as $q)
        {
            if($q['start_date'] <= strtotime('now') && $q['end_date'] >= strtotime('now'))
            {
                $quarter = $q;
            }
        }

        return $quarter;
    }
}
