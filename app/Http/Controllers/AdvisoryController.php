<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advisory;

class AdvisoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->isAdmin();
        $data = Advisory::all();
        return view('admin.advisory.index',compact('data'));
    }

    public function create()
    {
        // Check if it's admin
        $this->isAdmin();
        return view('admin.advisory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = new Advisory;
        $store->title = $request->title;
        $store->excerpt = $request->excerpt;
        $store->advisoryMessage = $request->advisoryMessage;
        
        if($request->published)
        {
            $updateAllStatus = Advisory::all();
            if($updateAllStatus)
            {
                foreach($updateAllStatus as $updateNow)
                {
                    $updateNow = Advisory::find($updateNow->_id);
                    $updateNow->published = 0;
                    $updateNow->save();
                }
            }
        }
        
        $store->published = ($request->published) ? 1 : 0;
        $store->save();
        
        return redirect(route('advisory.edit',['id'=>$store->_id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->isAdmin();
        $advisory = Advisory::find($id);
        return view('admin.advisory.edit',compact('advisory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $update = Advisory::find($id);
        $update->title = $request->title;
        $update->excerpt = $request->excerpt;
        $update->advisoryMessage = $request->advisoryMessage;

        if($request->published)
        {
            $updateAllStatus = Advisory::all();
            if($updateAllStatus)
            {
                foreach($updateAllStatus as $updateNow)
                {
                    $updateNow = Advisory::find($updateNow->_id);
                    $updateNow->published = 0;
                    $updateNow->save();
                }
            }
        }

        $update->published = ($request->published) ? 1 : 0;
        $update->save();
        return redirect(route('advisory.edit',['id'=>$update->_id]));
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $delete = Advisory::find($request->menu_id_delete);
        $delete->delete();
        return back();

    }
}
