<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->isAdmin();
        $data = User::with('role')->get();
        return view('admin.users.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->isAdmin();
        $roles = Role::all();
        return view('admin.users.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'name' => 'required'
        // ]);
        $fileurl = '';
        if($request->file('profile_pic')){
            $fileurl = $request->file('profile_pic')->store(
                'public/avatars'
            );
        }
        $user = new User;
        $user->profile_pic = $fileurl;
        $user->name = $request->name;
        $user->password = \Hash::make($request->password);
        $user->email = $request->email;
        $user->role_id = (object)$request->role;
        $user->status = ($request->status == "enabled") ? 1 : 0;
        $user->created_at = date('Y-m-d H:i:s');
        $user->updated_at = date('Y-m-d H:i:s');
        $user->save();

        return redirect(route('users.edit',compact('user')))->with('success','User Has Been Created!');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::all();
        
        $isAdmin = \Auth::user();
        $adminRole = Role::find($isAdmin->role_id);
        if($adminRole->name != 'Admin')
        {
            if($isAdmin->_id != $id)
            {
                abort(404);
            }
        }

        return view('admin.users.edit',compact('user','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $request->validate([
        //     'name' => 'required'
        // ]);
        $fileurl = '';
        if($request->file('profile_pic')){
            $fileurl = $request->file('profile_pic')->store(
                'public/avatars'
            );
        }else{
            $fileurl = $request->hidden_profile_pic;
        }

        $user = User::find($id);
        $user->profile_pic = $fileurl;
        $user->name = $request->name;
        $user->role_id = $request->role;
        $user->password = ($request->password) ? \Hash::make($request->password) : $request->hidden_password;
        $user->email = $request->email;
        $user->status = ($request->status == "enabled") ? 1 : 0;
        $user->updated_at = date('Y-m-d H:i:s');
        $user->save();

        $isAdmin = \Auth::user();
        $adminRole = Role::find($isAdmin->role_id);
        if($adminRole->name != 'Admin')
        {
            return redirect(route('dashboard'));

        }

        return redirect(route('users.edit',compact('user')))->with('success','User Has Been Edited!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->isAdmin();
        $user = User::find($request->menu_id_delete);
        $user->delete();
        return back()->with('success','User Has Been Deleted!');
    }
}
