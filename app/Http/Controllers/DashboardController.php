<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MerchantInfoImport;
use App\User;
use App\Role;
use App\Advisory;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $user = \Auth::user();
        $objRole = Role::find($user->role_id);
        $pAgent = $request->agent;

        //Check if admin and no parameter
        if($objRole->name == 'Admin')
        {
            $advisory = Advisory::where('published',1)->first();

            if($pAgent == null)
            {
                $user = User::where('role_id','!=',$objRole->_id)->get();
                $arrAgents = [];
                foreach($user as $u)
                {
                    $arrAgents[] = $this->get_agents_status($u);
                }
                return view('admin.dashboard.index',compact('arrAgents','advisory'));
            }
            else
            {
                $user = User::find($pAgent);
                $arrAgents = $this->get_agents_status($user);
                
                $objUsersList = User::where('status','=',1)->where('role_id','!=',$objRole->_id)->where('_id','!=',$pAgent)->get();
                $agents = []; 
                
                foreach($objUsersList as $userList)
                {
                    $merchantInfoReportAgents = \App\MerchantInfoImport::where('SALESPERSON','like','%'.$userList->name.'%')->groupBy('TITLE')->get(['TITLE','SALESPERSON','OVERALL SALES REVENUE']);
                    $agents[$userList->name]['count'] = $merchantInfoReportAgents->count();
                    $agents[$userList->name]['profile-pic'] = $userList->profile_pic;
                }
                return view('dashboard.index',compact('user','arrAgents','agents','advisory'));

            }
        } 
        else 
        {
            $arrAgents = $this->get_agents_status($user);
        }

        $roleAdmin = Role::where('name','Admin')->first();
        $objUsersList = User::where('status','=',1)->where('_id','!=',$user->_id)->where('role_id','!=',$roleAdmin->_id)->get();
        $agents = []; 
        
        foreach($objUsersList as $userList)
        {
            $merchantInfoReportAgents = \App\MerchantInfoImport::where('SALESPERSON','like','%'.$userList->name.'%')->groupBy('TITLE')->get(['TITLE','SALESPERSON','OVERALL SALES REVENUE']);
            $agents[$userList->name]['count'] = $merchantInfoReportAgents->count();
            $agents[$userList->name]['profile-pic'] = $userList->profile_pic;
        }

        $advisory = Advisory::where('published',1)->first();

        
        return view('dashboard.index',compact('user','arrAgents','agents','advisory'));
    }

    public function get_agents_status($user)
    {
        $merchantInfoReportAgents = \App\MerchantInfoImport::where('SALESPERSON','like','%'.$user->name.'%')->groupBy('TITLE')->get(['TITLE','SALESPERSON','OVERALL SALES REVENUE']);
        $agents['id'] = $user->_id;
        $agents['name'] = $user->name;
        $agents['count'] = $merchantInfoReportAgents->count();
        $agents['profile-pic'] = $user->profile_pic;
        $merchantSum = 0;

        foreach($merchantInfoReportAgents as $merc)
        {
            $merchantSum += (float)$merc['OVERALL SALES REVENUE'];
        }
        
        if($merchantSum > 999)
        {
            $strMerchantSum = ((string)$merchantSum)[0];
            $preNum = 'K';
        }

        if($merchantSum > 9999)
        {
            $strMerchantSum .= ((string)$merchantSum)[1];
            $preNum = 'K';
        }
        

        if($merchantSum > 99999)
        {
            $strMerchantSum .= ((string)$merchantSum)[2];
            $preNum = 'K';
        }

        if($merchantSum > 999999)
        {
            $strMerchantSum = ((string)$merchantSum)[0];
            $preNum = 'M';
        }

        if($merchantSum > 9999999)
        {
            $strMerchantSum .= ((string)$merchantSum)[1];
            $preNum = 'M';
        }

        if($merchantSum > 99999999)
        {
            $strMerchantSum .= ((string)$merchantSum)[2];
            $preNum = 'M';
        }

        $strMerchantSum .= $preNum;
        $agents['sales-revenue'] = $strMerchantSum;

        return $agents;
    }
    
    ///////////////////////////////////////////////////////////
    // SALES COUNT REPORT SECTION
    ///////////////////////////////////////////////////////////

    /** 
     * @param $requests
     * @return arrSales returns the data off for the graphs with filters
    */

    public function sales_count_report(Request $request)
    {
        
        $user = User::find($request->user_id);
        $merchantInfoReport = \App\MerchantInfoImport::where('SALESPERSON','like','%'.$user->name.'%')->orderBy('FIRST SALE DATE','ASC')->first();
        $first_date = $merchantInfoReport->{'FIRST SALE DATE'};
        
        if(isset($request->startDate))
        {
            $arrSales = $this->get_sales_by_custom_date($user,$request->startDate,$request->endDate,$request->settings);
        } else {
            if($request->settings == 'weekly')
            {
                $arrSales = $this->get_sales_by_week($user,$first_date);
            }
    
            if($request->settings == 'monthly')
            {
                $arrSales = $this->get_sales_by_month($user,$first_date);
            }

            if($request->settings == 'quarterly')
            {
                $arrSales = $this->get_sales_by_quarterly($user,$first_date);
            }
        }
        return $arrSales;
    }

    public function sales_count_report_admin(Request $request)
    {
        date_default_timezone_set('Australia/Melbourne');
        $roleAdmin = Role::where('name','Admin')->first();
        $users =  User::where('status','=',1)->where('role_id','!=',$roleAdmin->_id)->get();
        
        // $merchantInfoReport = \App\MerchantInfoImport::orderBy('FIRST SALE DATE','ASC')->first();
        // $first_date = $merchantInfoReport->{'FIRST SALE DATE'};
        $first_date = strtotime('January 1');
        
        $arrSales = [];
        foreach ($users as $key => $user) {
            
            if(isset($request->startDate))
            {
                $arrSales[] = array(
                    'count'=>$this->get_sales_by_custom_date($user,$request->startDate,$request->endDate,$request->settings),
                    'user' => $user->name
                );
            } else {
                if($request->settings == 'weekly')
                {
                    $arrSales[] = array(
                        'count'=>$this->get_sales_by_week($user,$first_date),
                        'user' => $user->name
                    );
                }
        
                if($request->settings == 'monthly')
                {
                    $arrSales[] = array(
                        'count'=>$this->get_sales_by_month($user,$first_date),
                        'user' => $user->name
                    );
                }

                if($request->settings == 'quarterly')
                {
                    $arrSales[] = array(
                        'count'=>$this->get_sales_by_quarterly($user,$first_date),
                        'user' => $user->name
                    );
                }
            }
        }
        return $arrSales;
    }

    private function get_sales_by_custom_date($aObjUser,$aDateStartDate,$aDateEndDate,$aStrSettings)
    {   
        $epochEndDate = strtotime($aDateEndDate);
        $epochStartDate = strtotime($aDateStartDate);

        if($aStrSettings == 'Daily')
        {
            $i=0;
            $ld = $epochEndDate;
            while($ld > $epochStartDate and $i<500)
            {
                $ld = strtotime(date('Y-m-d',$epochEndDate).' -'.$i.' day');
                $timeStart = strtotime(date('Y-m-d',$ld));
                $timeEnd = strtotime(date('Y-m-d 23:59:59',$ld));
                $merchantInfoReport = \App\MerchantInfoImport::where('SALESPERSON','like','%'.$aObjUser->name.'%')->where('FIRST SALE DATE','>=',$timeStart)->where('FIRST SALE DATE','<=',$timeEnd)->orderBy('FIRST SALE DATE','ASC')->count();
                $data[date('F d, Y',$ld)] = $merchantInfoReport;
                $i++;
            }
            return array_reverse($data);
        }

        if($aStrSettings == 'weekly')
        {
            $i=0;
            $ld = strtotime(date('Y-m-d',$epochEndDate).' -'.$i.' week +1 day');
            $lstart_week = strtotime("this week monday midnight",$ld);
            $lend_week = strtotime("this week sunday",$lstart_week);
            $lastWeekStart = date("Y-m-d",$lstart_week); 
            $lastWeekEnd = date("Y-m-d",$lend_week);
            $data = [];

            while ($lstart_week > $epochStartDate and $i<500) 
            {
                $ld = strtotime(date('Y-m-d',$epochEndDate).' -'.$i.' week +1 day');
                $lstart_week = strtotime("this week monday midnight",$ld);
                $lend_week = strtotime("this week sunday",$lstart_week);
                $lastWeekStart = date("Y-m-d",$lstart_week); 
                $lastWeekEnd = date("Y-m-d",$lend_week);
                $merchantInfoReport = \App\MerchantInfoImport::where('SALESPERSON','like','%'.$aObjUser->name.'%')->where('FIRST SALE DATE','>',strtotime($lastWeekStart))->where('FIRST SALE DATE','<',strtotime($lastWeekEnd))->orderBy('FIRST SALE DATE','ASC')->count();
                $data[date('M d, Y',$lstart_week).'-'.date('M d, Y',$lend_week)] = $merchantInfoReport;
                $i++;
            }
            return array_reverse($data);
        }

        if($aStrSettings == 'monthly')
        {
            $i=0;
            $ld = $epochEndDate;
            $firstDayOfTheMonth = strtotime(date('Y-m-1',$ld));
            $lastDayOfTheMonth = strtotime(date('Y-m-t',$ld));
            while($ld > $epochStartDate and $i<500)
            {
                $ld = strtotime(date('Y-m-25',$epochEndDate).' -'.$i.' month');
                $ldtye = date('Y-m-t',$ld);
                $firstDayOfTheMonth = strtotime(date('Y-m-1',$ld));
                $lastDayOfTheMonth = strtotime(date('Y-m-t',$ld));
                $merchantInfoReport = \App\MerchantInfoImport::where('SALESPERSON','like','%'.$aObjUser->name.'%')->where('FIRST SALE DATE','>',$firstDayOfTheMonth)->where('FIRST SALE DATE','<',$lastDayOfTheMonth)->orderBy('FIRST SALE DATE','ASC')->count();
                $data[date('F',$ld).' '.date('Y',$ld)] = $merchantInfoReport;
                $i++;
            }
            return array_reverse($data);
        }
    }

    private function get_sales_by_week($user,$first_date)
    {
        $i=0;
        $ld = strtotime("-".$i." week +1 day");
        $lstart_week = strtotime("this week monday midnight",$ld);
        $lend_week = strtotime("this week sunday",$lstart_week);
        $lastWeekStart = date("Y-m-d",$lstart_week); 
        $lastWeekEnd = date("Y-m-d",$lend_week);
        $data = [];
        while ($lstart_week > $first_date and $i<500) 
        {
            $ld = strtotime("-".$i." week +1 day");
            $lstart_week = strtotime("this week monday midnight",$ld);
            $lend_week = strtotime("this week sunday",$lstart_week);
            $lastWeekStart = date("Y-m-d",$lstart_week); 
            $lastWeekEnd = date("Y-m-d",$lend_week);
            $merchantInfoReport = \App\MerchantInfoImport::where('SALESPERSON','like','%'.$user->name.'%')->where('FIRST SALE DATE','>',strtotime($lastWeekStart))->where('FIRST SALE DATE','<',strtotime($lastWeekEnd))->orderBy('FIRST SALE DATE','ASC')->count();
            $data['Week '.date('W',$lstart_week).'('.date('Y',$lstart_week).')'] = $merchantInfoReport;
		    $i++;
        }
        return array_reverse($data);
    }

    private function get_sales_by_month($user,$first_date)
    {
        $i=0;
        $ld = strtotime("now");
        $firstDayOfTheMonth = strtotime(date('Y-m-1',$ld));
        $lastDayOfTheMonth = strtotime(date('Y-m-t',$ld));
        while($ld > $first_date and $i<500)
        {
            $ld = strtotime("-".$i." month now");
            $firstDayOfTheMonth = strtotime(date('Y-m-1',$ld));
            $lastDayOfTheMonth = strtotime(date('Y-m-t',$ld));
            $merchantInfoReport = \App\MerchantInfoImport::where('SALESPERSON','like','%'.$user->name.'%')->where('FIRST SALE DATE','>',$firstDayOfTheMonth)->where('FIRST SALE DATE','<',$lastDayOfTheMonth)->orderBy('FIRST SALE DATE','ASC')->count();
            $data[date('F',$ld).' '.date('Y',$ld)] = $merchantInfoReport;
            $i++;
        }
        return array_reverse($data);
    }

    private function get_sales_by_quarterly($user,$first_date)
    {
        
        $quarters = [
            [
                'quarter' => 'Q1',
                'start_date' => strtotime('july 1'),
                'end_date' => strtotime('september 30')
            ],

            [
                'quarter' => 'Q2',
                'start_date' => strtotime('october 1'),
                'end_date' => strtotime('december 31')
            ],

            [
                'quarter' => 'Q3',
                'start_date' => strtotime('january 1 +1 year'),
                'end_date' => strtotime('march 31 +1 year')
            ],

            [
                'quarter' => 'Q4',
                'start_date' => strtotime('april 1 +1 year'),
                'end_date' => strtotime('june 30 +1 year')
            ]
        ];

        foreach($quarters as $q)
        {
            $merchantInfoReport = \App\MerchantInfoImport::where('SALESPERSON','like','%'.$user->name.'%')->where('FIRST SALE DATE','>',$q['start_date'])->where('FIRST SALE DATE','<',$q['end_date'])->orderBy('FIRST SALE DATE','ASC')->count();
            $data[$q['quarter'].' ('.date('F Y',$q['start_date']).' - '.date('F Y',$q['end_date']).')'] = $merchantInfoReport;
        }
        
        return $data;
    }
    ///////////////////////////////////////////////////////////
    // SALES COUNT REPORT SECTION
    ///////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////
    // SALES PER REGION REPORT SECTION
    ///////////////////////////////////////////////////////////
    public function sales_per_region(Request $request)
    {
        $user = User::find($request->user_id);
        $merchantInfoReport = \App\MerchantInfoImport::where('SALESPERSON','like','%'.$user->name.'%')->where('COUNTRY',$request->country)->whereNotNull('ADMINISTRATIVE AREA')->where('ADMINISTRATIVE AREA','!=','')->groupBy('ADMINISTRATIVE AREA')->get(['ADMINISTRATIVE AREA','COUNTRY'])->sortBy(['ADMINISTRATIVE AREA']);
        $intAdministrativeAreaCount = [];
        foreach($merchantInfoReport as $admins)
        {
            $intAdministrativeAreaCount[] = array(
                'administrative_area' => $admins->{'ADMINISTRATIVE AREA'},
                'count'=> \App\MerchantInfoImport::where('SALESPERSON','like','%'.$user->name.'%')->where('COUNTRY',$request->country)->where('ADMINISTRATIVE AREA',$admins->{'ADMINISTRATIVE AREA'})->count(),
                'country' => $admins->{'COUNTRY'},
            );
        }

        $countries = \App\MerchantInfoImport::where('SALESPERSON','like','%'.$user->name.'%')->whereNotNull('ADMINISTRATIVE AREA')->where('ADMINISTRATIVE AREA','!=','')->groupBy('COUNTRY')->get(['ADMINISTRATIVE AREA','COUNTRY'])->sortBy(['COUNTRY']);
        $bansa = [];
        foreach ($countries as $key => $country) {
            $bansa[] = $country->{'COUNTRY'};
        }
    
        $data = array(
            'intAdministrativeAreaCount' => $intAdministrativeAreaCount,
            'countries' => $bansa
        );

        return $data;
    }
    ///////////////////////////////////////////////////////////
    // SALES PER REGION REPORT SECTION
    ///////////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////////
    // MERCHANT INFO SECTION
    ///////////////////////////////////////////////////////////

    public function merchant_info(Request $request)
    {
        $user = User::find($request->user_id);
        
        if($request->advance != 'advance')  
        {
            $merchantInfoReport = \App\MerchantInfoImport::where('SALESPERSON','like','%'.$user->name.'%')->groupBy('TITLE')->orderBy('FIRST SALE DATE','DESC')->take(5)->get(['TITLE','OWNER\'S MOBILE NUMBER','OVERALL SALES REVENUE']);
            $arr = [];
            foreach($merchantInfoReport as $info)
            {
                $arr[] = [
                    'title'=>$info->{'TITLE'},
                    'mobile_number'=>$info->{'OWNER\'S MOBILE NUMBER'},
                    'overall_sales_revnue'=>$info->{'OVERALL SALES REVENUE'},
                ];
            }
        } else {
            $merchantInfoReport = \App\MerchantInfoImport::where('SALESPERSON','like','%'.$user->name.'%')->groupBy('TITLE')->orderBy('FIRST SALE DATE','DESC')->get(['TITLE','OWNER\'S EMAIL ADDRESS','OWNER\'S MOBILE NUMBER','FIRST SALE DATE','OVERALL SALES REVENUE']);
            $arr = [];
            foreach($merchantInfoReport as $info)
            {
                $arr[] = [
                    'title'=>$info->{'TITLE'},
                    'email_address'=>$info->{'OWNER\'S EMAIL ADDRESS'},
                    'mobile_number'=>$info->{'OWNER\'S MOBILE NUMBER'},
                    'first_sale_date'=>(int)$info->{'FIRST SALE DATE'},
                    'overall_sales_revnue'=>$info->{'OVERALL SALES REVENUE'}
                ];
            }
        }
        return array('data'=>$arr);
    }

    private function get_current_quarter()
    {
        date_default_timezone_set('Australia/Melbourne');
        $quarters = [
            [
                'quarter' => 'Q1',
                'start_date' => strtotime('july 1'),
                'end_date' => strtotime('september 30')
            ],

            [
                'quarter' => 'Q2',
                'start_date' => strtotime('october 1'),
                'end_date' => strtotime('december 31')
            ],

            [
                'quarter' => 'Q3',
                'start_date' => strtotime('january 1'),
                'end_date' => strtotime('march 31')
            ],

            [
                'quarter' => 'Q4',
                'start_date' => strtotime('april 1'),
                'end_date' => strtotime('june 30')
            ]
        ];
        
        $quarter = '';
        
        foreach($quarters as $q)
        {
            if($q['start_date'] <= strtotime('now') && $q['end_date'] >= strtotime('now'))
            {
                $quarter = $q;
            }
        }

        return $quarter;
    }
}
