<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Menu::all();

        return view('admin.menu.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {  
        $create = new Menu;
        $create->menu_name = $request->menu_name;
        $create->menu_key = preg_replace('/[^a-zA-Z0-9_-]/','-',strtolower($request->menu_name));
        $create->enabled = 1;
        $create->created_at = date('Y-m-d h:i:s');
        $create->updated_at = date('Y-m-d h:i:s');
        $create->save();
        return json_encode(true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $update = Menu::find($request->menu_id);
        $update->menu_name = $request->menu_name;
        $update->menu_key = preg_replace('/[^a-zA-Z0-9_-]/','-',strtolower($request->menu_name));
        $update->enabled = 1;
        $update->created_at = date('Y-m-d h:i:s');
        $update->updated_at = date('Y-m-d h:i:s');
        $update->save();
        return json_encode(true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {  
        $delete = Menu::find($request->menu_id_delete);
        $delete->delete();
        return redirect(route('menus'))->with('success','Menu Has Been Deleted!');
    }

    public function menu_items(Request $request){
        $data = Menu::find($request->menu_id);        
        return view('admin.menu.menu-items',compact('data'));
    }

    public function menu_items_store(){ 
         
    }
}
