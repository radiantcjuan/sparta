<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Ixudra\Curl\Facades\Curl;
use App\MerchantInfoImport;

class MigrateAccountInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'MigrateAccountInfo:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // $csv = file_get_contents('https://admin.ordermate.online/reports/merchant-info?export=true');
        // dd($csv);
    
        // $merchantInfo = MerchantInfoImport::all();

        // foreach($merchantInfo as $merchants)
        // {
        //     $update = MerchantInfoImport::find($merchants->_id);
        //     $update->{'DATE CREATED'} = strtotime($merchants->{'DATE CREATED'});
        //     $update->{'FIRST SALE DATE'} = strtotime($merchants->{'FIRST SALE DATE'});
        //     $update->{'LAST SALE DATE'} = strtotime($merchants->{'LAST SALE DATE'});
        //     $update->{'OVERALL SALES REVENUE'} = (int)($merchants->{'OVERALL SALES REVENUE'});
        //     $update->save();
        // }
    }   
}
