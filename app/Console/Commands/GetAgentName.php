<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\MerchantInfoImport;

class GetAgentName extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'MigrateAccountInfo:agents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return MerchantInfo Json
     */
    public function handle()
    {
        $MerchantInfo  = MerchantInfoImport::all();
        foreach($MerchantInfo as $MerchantInf)
        {
            if($MerchantInf->SALESPERSON)
            {
                $user = User::where('name',$MerchantInf->SALESPERSON)->get();
                if($user->isEmpty())
                {
                    $user = new User;
                    $user->name = $MerchantInf->SALESPERSON;
                    $user->password = \Hash::make('p@550w0rd');
                    $user->email = str_replace(' ','',strtolower($MerchantInf->SALESPERSON)).'@ordermate.com.au';
                    $user->status = 1;
                    $user->created_at = date('Y-m-d H:i:s');
                    $user->updated_at = date('Y-m-d H:i:s');
                    $user->save();
                }
            }
        }
        return $MerchantInfo;
    }
}