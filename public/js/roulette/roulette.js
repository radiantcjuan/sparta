/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 233);
/******/ })
/************************************************************************/
/******/ ({

/***/ 233:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(234);


/***/ }),

/***/ 234:
/***/ (function(module, exports) {

$(document).ready(function () {
	// $('.roulette').find('img').hover(function(){
	// 	console.log($(this).height());
	// });
	// var appendLogMsg = function(msg) {
	// 	$('#msg')
	// .append('<p class="muted">' + msg + '</p>')
	// .scrollTop(100000000);

	// }
	var p = {
		startCallback: function startCallback() {
			// appendLogMsg('start');
			// $('#speed, #duration').slider('disable');
			// $('#stopImageNumber').spinner('disable');
			// $('.start').attr('disabled', 'true');
			// $('.stop').removeAttr('disabled');


		},
		slowDownCallback: function slowDownCallback() {
			// appendLogMsg('slowdown');
			// $('.stop').attr('disabled', 'true');
		},
		stopCallback: function stopCallback($stopElm) {
			///AJAX KA DITO
		}
	};
	var rouletter = $('div.roulette');
	rouletter.roulette(p);

	var csrfToken = $('input[name="_token"]').val();
	var roundtype = $('input[name="roundtype"]').val();
	$('.start').click(function () {
		$(this).css({
			'background-image': 'url(/images/leve-down.png)',
			'width': '47',
			'right': '128px'
		});

		var lever = document.getElementById("lever-sound");
		lever.play();

		var spinning = document.getElementById("spinning-sound");
		spinning.play();

		$winner = 0;
		$.ajax({
			url: '/roulette/winner/' + roundtype,
			type: 'POST',
			headers: {
				'X-CSRF-TOKEN': csrfToken
			},
			success: function success(res) {
				$winner = res['agent'];
				p['speed'] = 15;
				p['duration'] = 5.8;
				p['stopImageNumber'] = $winner;
				rouletter.roulette('option', p);
				rouletter.roulette('start');

				$.ajax({
					url: '/roulette/register-winner/' + roundtype,
					type: 'POST',
					data: { agent: res['agent_name'] },
					headers: {
						'X-CSRF-TOKEN': csrfToken
					},
					success: function success(res) {
						console.log(res);
					}
				});
			}
		});
	});

	// var updateParamater = function(){
	// 	p['speed'] = 7;
	// 	p['duration'] = Number($('.duration_param').eq(0).text());
	// 	p['stopImageNumber'] = Number($('.stop_image_number_param').eq(0).text());
	// 	rouletter.roulette('option', p);	
	// }
	// var updateSpeed = function(speed){
	// 	$('.speed_param').text(speed);
	// }
	// $('#speed').slider({
	// 	min: 1,
	// 	max: 30,
	// 	value : 10,
	// 	slide: function( event, ui ) {
	// 		updateSpeed(ui.value);
	// 		updateParamater();
	// 	}
	// });
	// updateSpeed($('#speed').slider('value'));

	// var updateDuration = function(duration){
	// 	$('.duration_param').text(duration);
	// }
	// $('#duration').slider({
	// 	min: 2,
	// 	max: 10,
	// 	value : 3,
	// 	slide: function( event, ui ) {
	// 		updateDuration(ui.value);
	// 		updateParamater();
	// 	}
	// });
	// updateDuration($('#duration').slider('value'));

	// var updateStopImageNumber = function(stopImageNumber) {
	// 	$('.image_sample').children().css('opacity' , 0.2);
	// 	$('.image_sample').children().filter('[data-value="' + stopImageNumber + '"]').css('opacity' , 1);
	// 	$('.stop_image_number_param').text(stopImageNumber);
	// 	updateParamater();
	// }

	// $('#stopImageNumber').spinner({
	// 	spin: function( event, ui ) {
	// 		var imageNumber = ui.value;
	// 		if ( ui.value > 4 ) {
	// 			$( this ).spinner( "value", -1 );
	// 			imageNumber = 0;	
	// 			updateStopImageNumber(-1);		
	// 			return false;
	// 		} else if ( ui.value < -1 ) {
	// 			$( this ).spinner( "value", 4 );
	// 			imageNumber = 4;	
	// 			updateStopImageNumber(4);		
	// 			return false;
	// 		}
	// 		updateStopImageNumber(imageNumber);		
	// 	}
	// });
	// $('#stopImageNumber').spinner('value', 0);
	// updateStopImageNumber($('#stopImageNumber').spinner('value'));		

	// $('.image_sample').children().click(function(){
	// 	var stopImageNumber = $(this).attr('data-value');
	// 	$('#stopImageNumber').spinner('value', stopImageNumber);
	// 	updateStopImageNumber(stopImageNumber);
	// });
});

/***/ })

/******/ });