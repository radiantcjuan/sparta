/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 229);
/******/ })
/************************************************************************/
/******/ ({

/***/ 229:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(230);


/***/ }),

/***/ 230:
/***/ (function(module, exports) {

$(document).ready(function () {
    var csrfToken = $('input[name="_token"]').val();
    var userId = $('input[name="userId"]').val();
    var defaultCountry = "AU";

    function random_rgba() {
        var hover = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

        var o = Math.round,
            r = Math.random,
            s = 255;
        var oppa = !hover ? 0.5 : 1;
        return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ',' + oppa + ')';
    }

    $.ajax({
        url: '/api/dashboard/sales-per-region',
        type: 'POST',
        data: { user_id: userId, country: defaultCountry },
        headers: {
            'X-CSRF-TOKEN': csrfToken
        },
        success: function success(res) {
            // PIE CHART
            console.log(res['intAdministrativeAreaCount']);
            var label = [];
            var value = [];
            var colorArr = [];
            var colorArrhOVER = [];

            for (var adArea in res['intAdministrativeAreaCount']) {
                var color = random_rgba();
                label.push(res['intAdministrativeAreaCount'][adArea].administrative_area);
                value.push(res['intAdministrativeAreaCount'][adArea].count);
                colorArr.push(color);
                colorArrhOVER.push('#ee4044');
                $('.chart-legend .pt-2').append('<li class="px-1"><i class="fa fa-circle" style="color: ' + color + '"></i> ' + res['intAdministrativeAreaCount'][adArea].administrative_area + '</li>');
            }

            for (var keyCoinutry in res['countries']) {
                $('.dd-filter-by-country').append('<a class="dropdown-item dd-filter-by-country-item ' + (defaultCountry == res['countries'][keyCoinutry] ? 'active' : '') + '" href="javascript:void(0)" data-settings="' + res['countries'][keyCoinutry] + '">' + res['countries'][keyCoinutry] + '</a>');
            }

            var pie = document.getElementById('salesPerRegion').getContext('2d');
            var data = {
                datasets: [{
                    data: value,
                    label: "Sales Per Region",
                    backgroundColor: colorArr,
                    borderColor: 'rgba(255,255,255,1)',
                    borderWidth: 0,
                    hoverBackgroundColor: colorArrhOVER,
                    hoverBorderColor: 'rgba(255,255,255,1)',
                    hoverBorderWidth: 1
                }],

                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: label
            };

            var myPieChart = new Chart(pie, {
                type: 'pie',
                data: data,
                maintainAspectRatio: false,

                options: {
                    legend: {
                        display: false
                    }
                }
            });
        },
        error: function error(err) {
            console.log(err);
        }

    });

    $(document).on('click', '.dd-filter-by-country-item', function () {
        defaultCountry = $(this).data('settings');
        $.ajax({
            url: '/api/dashboard/sales-per-region',
            type: 'POST',
            data: { user_id: userId, country: defaultCountry },
            headers: {
                'X-CSRF-TOKEN': csrfToken
            },
            success: function success(res) {
                // PIE CHART
                console.log(res['intAdministrativeAreaCount']);
                var label = [];
                var value = [];
                var colorArr = [];
                var colorArrhOVER = [];
                $('.chart-legend .pt-2').html('');
                for (var adArea in res['intAdministrativeAreaCount']) {
                    var color = random_rgba();
                    label.push(res['intAdministrativeAreaCount'][adArea].administrative_area);
                    value.push(res['intAdministrativeAreaCount'][adArea].count);
                    colorArr.push(color);
                    colorArrhOVER.push('#ee4044');
                    $('.chart-legend .pt-2').append('<li class="px-1"><i class="fa fa-circle" style="color: ' + color + '"></i> ' + res['intAdministrativeAreaCount'][adArea].administrative_area + '</li>');
                }

                $('.dd-filter-by-country').html('');
                for (var keyCoinutry in res['countries']) {
                    $('.dd-filter-by-country').append('<a class="dropdown-item dd-filter-by-country-item ' + (defaultCountry == res['countries'][keyCoinutry] ? 'active' : '') + '" href="javascript:void(0)" data-settings="' + res['countries'][keyCoinutry] + '">' + res['countries'][keyCoinutry] + '</a>');
                }

                $('#salesPerRegion').remove();
                $('.chart-container').append('<canvas id="salesPerRegion"></canvas>');

                var pie = document.getElementById('salesPerRegion').getContext('2d');
                var data = {
                    datasets: [{
                        data: value,
                        label: "Sales Per Region",
                        backgroundColor: colorArr,
                        borderColor: 'rgba(255,255,255,1)',
                        borderWidth: 0,
                        hoverBackgroundColor: colorArrhOVER,
                        hoverBorderColor: 'rgba(255,255,255,1)',
                        hoverBorderWidth: 1
                    }],

                    // These labels appear in the legend and in the tooltips when hovering different arcs
                    labels: label
                };

                var myPieChart = new Chart(pie, {
                    type: 'pie',
                    data: data,
                    maintainAspectRatio: false,

                    options: {
                        legend: {
                            display: false
                        }
                    }
                });
            },
            error: function error(err) {
                console.log(err);
            }

        });
    });
});

/***/ })

/******/ });