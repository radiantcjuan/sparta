/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 231);
/******/ })
/************************************************************************/
/******/ ({

/***/ 231:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(232);


/***/ }),

/***/ 232:
/***/ (function(module, exports) {

$(document).ready(function () {
    var userId = $('input[name="userId"]').val();

    $('#merchantInfoReport').DataTable({
        "lengthMenu": false,
        "paging": false,
        "searching": false,
        "info": false,
        "ajax": "/api/dashboard/merchant-info/not-advance/" + userId,
        "columns": [{ "data": "title" }, { "data": "mobile_number" }, { "data": "overall_sales_revnue" }]
    });

    $('#merchantInfoReportModal').DataTable({
        "ajax": "/api/dashboard/merchant-info/advance/" + userId,
        "columns": [{ "data": "title" }, { "data": "email_address" }, { "data": "mobile_number" }, {
            "data": "first_sale_date",
            "render": function render(data, type) {
                if (type == 'sort' || type == 'type') {
                    return data;
                }
                return moment.unix(data).format('DD MMM YYYY');
            }
        }, { "data": "overall_sales_revnue" }]
    });

    $('.merchant-info-view-more').click(function () {
        $('#merchinInfoViewMore').modal('show');
    });
});

/***/ })

/******/ });