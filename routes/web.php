<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function(){
    return redirect(route('dashboard'));
})->middleware('auth');

//USERS
Route::get('users','UserController@index')->middleware('auth')->name('users.index');
Route::group(['prefix'=>'users'],function(){
    Route::get('create', 'UserController@create')->name('users.create');
    Route::post('store', 'UserController@store')->name('users.store');
    Route::get('{id}/edit', 'UserController@edit')->name('users.edit');
    Route::put('{id}/edit', 'UserController@update')->name('users.edit');
    Route::delete('delete', 'UserController@destroy')->name('users.delete');
});

// ROULETTE
Route::get('roulette/{round}','RouletteController@index')->middleware('auth')->name('roulette');
Route::post('roulette/winner/{round}','RouletteController@get_winner');
Route::post('roulette/register-winner/{round}','RouletteController@register_winner');


//DASHBOARD
Route::get('dashboard/{agent?}','DashboardController@index')->middleware('auth')->name('dashboard');
Route::group(['prefix'=>'api'],function(){
    Route::post('dashboard/sales-count/{settings}','DashboardController@sales_count_report');
    Route::post('dashboard/sales-count-admin/{settings}','DashboardController@sales_count_report_admin');
    Route::post('dashboard/sales-per-region','DashboardController@sales_per_region');
    Route::get('dashboard/merchant-info/{advance}/{user_id}','DashboardController@merchant_info');
});


//LEADERBOARD
Route::get('leaderboard/{type}','LeaderBoardController@index')->name('leaderboard');

//ADVISORIES
Route::get('advisories','AdvisoryController@index')->name('advisory.index');
Route::group(['prefix'=>'advisories'],function(){
    Route::get('create', 'AdvisoryController@create')->name('advisory.create');
    Route::post('store', 'AdvisoryController@store')->name('advisory.store');
    Route::get('{id}/edit', 'AdvisoryController@edit')->name('advisory.edit');
    Route::put('{id}/edit', 'AdvisoryController@update')->name('advisory.update');
    Route::delete('delete', 'AdvisoryController@destroy')->name('advisory.delete');
});

//AUTHENTICATION
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login-user', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');