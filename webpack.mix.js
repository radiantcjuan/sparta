let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/scss/app.scss', 'public/css')
   .js('resources/assets/js/dashboard/dashboard.js', 'public/js/dashboard/')
   .js('resources/assets/js/dashboard/sales_count.js', 'public/js/dashboard/')
   .js('resources/assets/js/dashboard/sales_per_region.js', 'public/js/dashboard/')
   .js('resources/assets/js/dashboard/merchant_info.js', 'public/js/dashboard/')
   .js('resources/assets/js/roulette/roulette.js', 'public/js/roulette/')
   .js('resources/assets/js/admin/user.js', 'public/js/admin/')
   .js('resources/assets/js/admin/advisory.js', 'public/js/admin/');

